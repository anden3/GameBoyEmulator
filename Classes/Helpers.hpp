#pragma once

#include <string>
#include <vector>
#include <iomanip>
#include <sstream>

std::vector<std::string> split(const std::string &s, char delim = ' ');
std::string join(const std::vector<std::string> &vec, const char* delim = " ");

template <class Container, typename T>
bool contains(Container &cont, T item) {
    return std::find(cont.begin(), cont.end(), item) != cont.end();
}

template <typename T> 
constexpr void rotate_left(T &val) {
    static_assert(std::is_unsigned<T>::value, "Rotate Left only makes sense for unsigned types");
    val = static_cast<T>((val << 1) | (val >> (sizeof(T) * 8 - 1)));
}

template <typename T> 
constexpr void rotate_right(T &val) {
    static_assert(std::is_unsigned<T>::value, "Rotate Right only makes sense for unsigned types");
    val = static_cast<T>((val >> 1) | (val << (sizeof(T) * 8 - 1)));
}

template<typename T>
std::string to_hex(T i, bool prefix = true) {
    std::stringstream stream;

    if (prefix) {
        stream << "0x";
    }

    stream << std::setfill('0') << std::setw(sizeof(T) * 2) << std::hex << std::uppercase << static_cast<int>(i);
    return stream.str();
}

template<typename T>
T str_to_num(std::string s, bool base = 0) {
    return static_cast<T>(std::stoul(s, nullptr, base));
}
