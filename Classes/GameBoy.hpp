#pragma once

#include <string>
#include <vector>
#include <cstdint>

typedef uint8_t  Byte;
typedef uint16_t Word;

class CPU;
class Memory;
class Display;

class GameBoy {
public:
	GameBoy();
	~GameBoy();

	void Start();
	void Initialize(std::string path);
	void Load_ROM(std::string path);

	void Signal_Handler(int signal);

private:
	CPU* cpu;
	Memory* memory;
	Display* display;

	bool CGBOnly;
	bool CGBFunctions;
	bool SGBFunctions;
	bool JapaneseOnly;

	int RAMSize;
	int ROMSize;

	Byte GameVersion;

	union {
		Byte PublisherByte;
		char PublisherCode[2];
	};

	std::string ROMTitle;
	std::string ManufacturerCode;
	
	std::vector<Byte> Cartridge;

	void Check_Integrity();
};
