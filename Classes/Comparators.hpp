#pragma once

#include <glm/glm.hpp>

class VectorComparator {
public:
	template <typename T>
	bool operator() (const glm::tvec2<T> &a, const glm::tvec2<T> &b) const {
		if (a.x != b.x) { return a.x < b.x; };
		if (a.y != b.y) { return a.y < b.y; };
		return false;
	}

	template <typename T>
	bool operator() (const glm::tvec3<T> &a, const glm::tvec3<T> &b) const {
		if (a.x != b.x) { return a.x < b.x; };
		if (a.y != b.y) { return a.y < b.y; };
		if (a.z != b.z) { return a.z < b.z; };
		return false;
	}

	template <typename T>
	bool operator() (const glm::tvec4<T> &a, const glm::tvec4<T> &b) const {
		if (a.x != b.x) { return a.x < b.x; };
		if (a.y != b.y) { return a.y < b.y; };
		if (a.z != b.z) { return a.z < b.z; };
		if (a.w != b.w) { return a.w < b.w; };
		return false;
	}
};
