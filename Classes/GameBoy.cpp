#include "Gameboy.hpp"

#include <iostream>

#include "CPU.hpp"
#include "Memory.hpp"
#include "Display.hpp"

enum CartridgeTypes {
	ROM, RAM, SRAM, BATTERY, RUMBLE, CAMERA, TIMER, SENSOR,
	MBC1, MBC2, MBC3, MBC5, MBC6, MBC7,
	MMM01, TAMA5, HUC1, HUC3
};

const std::map<Byte, std::vector<Byte>> Cartridges = {
	{0x00, {ROM}},
	{0x01, {ROM, MBC1}},
	{0x02, {ROM, MBC1, RAM}},
	{0x03, {ROM, MBC1, RAM, BATTERY}},
	{0x05, {ROM, MBC2}},
	{0x06, {ROM, MBC2, BATTERY}},
	{0x08, {ROM, RAM}},
	{0x0B, {ROM, MMM01}},
	{0x0C, {ROM, MMM01, SRAM}},
	{0x0D, {ROM, MMM01, SRAM, BATTERY}},
	{0x0F, {ROM, MBC3, TIMER, BATTERY}},
	{0x10, {ROM, MBC3, TIMER, RAM, BATTERY}},
	{0x11, {ROM, MBC3}},
	{0x12, {ROM, MBC3, RAM}},
	{0x13, {ROM, MBC3, RAM, BATTERY}},
	{0x19, {ROM, MBC5}},
	{0x1A, {ROM, MBC5, RAM}},
	{0x1B, {ROM, MBC5, RAM, BATTERY}},
	{0x1C, {ROM, MBC5, RUMBLE}},
	{0x1D, {ROM, MBC5, RUMBLE, SRAM}},
	{0x1E, {ROM, MBC5, RUMBLE, SRAM, BATTERY}},
	{0x20, {ROM, MBC6}},
	{0x22, {ROM, MBC7, SENSOR, RUMBLE, RAM, BATTERY}},
	{0xFC, {CAMERA}},
	{0xFD, {TAMA5}},
	{0xFE, {HUC3}},
	{0xFF, {HUC1, RAM, BATTERY}}
};

const std::map<Byte, int> ROMSizes = {
	{0x00,   32 * 1000},
	{0x01,   64 * 1000},
	{0x02,  128 * 1000},
	{0x03,  256 * 1000},
	{0x04,  512 * 1000},
	{0x05,    1 * 1000 * 1000},
	{0x06,    2 * 1000 * 1000},
	{0x07,    4 * 1000 * 1000},
	{0x08,    8 * 1000 * 1000},

	{0x52,  1.1 * 1000 * 1000},
	{0x53,  1.2 * 1000 * 1000},
	{0x54,  1.5 * 1000 * 1000}
};

const std::map<Byte, int> RAMSizes = {
	{0x00,    0},
	{0x01,    2 * 1000},
	{0x02,    8 * 1000},
	{0x03,   32 * 1000},
	{0x04,  128 * 1000},
	{0x05,   64 * 1000}
};

GameBoy::GameBoy() {
	cpu     = new CPU();
	memory  = new Memory();
	display = new Display();
}

GameBoy::~GameBoy() {
	delete cpu;
	delete memory;
	delete display;
}

void GameBoy::Initialize(std::string path) {
	memory ->Initialize(cpu, display);
    cpu    ->Initialize(memory, display);
    display->Initialize(cpu, memory);

    memory->Map_Bootstrap(path);
}

void GameBoy::Load_ROM(std::string path) {
	if (!memory->Load_ROM(path)) {
		return;
	}

    // Title
   	ROMTitle = std::string(memory->Get_Byte_Pointer(0x134), memory->Get_Byte_Pointer(0x143)).data();

   	// Manufacturer
   	ManufacturerCode = std::string(memory->Get_Byte_Pointer(0x13F), memory->Get_Byte_Pointer(0x142)).data();

   	// CGB
   	CGBFunctions = memory->Read_Byte(0x143) & 0x80;
   	CGBOnly      = memory->Read_Byte(0x143) & 0x40;

   	// SGB
   	switch (memory->Read_Byte(0x146)) {
   		case 0x03:
   			SGBFunctions = true;
   			break;

   		default:
   			SGBFunctions = false;
   	}

   	// Cartridge
   	Byte cartridge = memory->Read_Byte(0x147);

   	if (!Cartridges.count(cartridge)) {
   		std::cerr << "ERROR! UNKNOWN CARTRIDGE TYPE.\n";
   		return;
   	}

   	Cartridge = Cartridges.at(cartridge);

   	// ROM Size
   	Byte romVal = memory->Read_Byte(0x148);

   	if (!ROMSizes.count(romVal)) {
   		std::cerr << "ERROR! UNKNOWN ROM FLAG.\n";
   		return;
   	}

   	ROMSize = ROMSizes.at(romVal);

   	// RAM Size
   	Byte ramVal = memory->Read_Byte(0x149);

   	if (!RAMSizes.count(ramVal)) {
   		std::cerr << "ERROR! UNKNOWN RAM FLAG.\n";
   		return;
   	}

   	RAMSize = ROMSizes.at(ramVal);

   	// Japanese only
   	switch (memory->Read_Byte(0x14A)) {
   		case 0x00:
   			JapaneseOnly = true;
   			break;

   		case 0x01:
   			JapaneseOnly = false;
   			break;

   		default:
   			std::cerr << "ERROR! UNKNOWN JAPANESE FLAG.\n";
   			return;
   	}

   	Byte pubByte = memory->Read_Byte(0x14B);

   	if (pubByte == 0x33) {
   		PublisherCode[0] = memory->Read_Byte(0x144);
   		PublisherCode[1] = memory->Read_Byte(0x145);
   	}
   	else {
   		PublisherByte = pubByte;
   	}

   	// Game version
   	GameVersion = memory->Read_Byte(0x14C);

    Check_Integrity();
}

void GameBoy::Check_Integrity() {
	// Check Nintendo graphic
	static const std::vector<Byte> nintendoReference = {
		0xCE, 0xED, 0x66, 0x66, 0xCC, 0x0D, 0x00, 0x0B, 0x03, 0x73, 0x00, 0x83, 0x00, 0x0C, 0x00, 0x0D,
 		0x00, 0x08, 0x11, 0x1F, 0x88, 0x89, 0x00, 0x0E, 0xDC, 0xCC, 0x6E, 0xE6, 0xDD, 0xDD, 0xD9, 0x99,
 		0xBB, 0xBB, 0x67, 0x63, 0x6E, 0x0E, 0xEC, 0xCC, 0xDD, 0xDC, 0x99, 0x9F, 0xBB, 0xB9, 0x33, 0x3E
	};

	std::vector<Byte> data(memory->Get_Byte_Pointer(0x104), memory->Get_Byte_Pointer(0x134));

	if (data != nintendoReference) {
		std::cerr << "ERROR! SCROLLING NINTENDO GRAPHIC CORRUPT IN ROM.\n";
		return;
	}

	// Header checksum
	int x = 0;

	for (Word i = 0x134; i <= 0x14C; ++i) {
		x -= memory->Read_Byte(i) + 1;
	}

	if ((x & 0xF) != memory->Read_Byte(0x14D)) {
		std::cerr << "ERROR! HEADER CHECKSUM NOT VALID.\n";
		return;
	}
}

void GameBoy::Start() {
	cpu->Dispatch_Loop();
}

void GameBoy::Signal_Handler(int signal) {
	cpu->Signal_Handler(signal);
}
