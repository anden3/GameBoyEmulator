#pragma once

#include <map>
#include <vector>
#include <cstdint>

#include "Comparators.hpp"

typedef uint8_t  Byte;
typedef uint16_t Word;

class CPU;
class Memory;

struct Tile;

struct ColorPalette;
struct SpritePalette;
struct SpriteAttribute;

struct DisplayStatus;
struct DisplayControl;

class Display {
public:
	Display();

	void Initialize(CPU* c, Memory* mem);
	void Process();
	void Reset();

	void DMA_Transfer(Byte source);

private:
	CPU* cpu;
	Memory* memory;

	bool VBlank = false;

	Byte* ScrollX;
	Byte* ScrollY;

	Byte* WindowX;
	Byte* WindowY;

	Byte* LY;

	Word CurrentLineStart = 0;
	Word CurrentLineCycles = 0;

	DisplayStatus*  Status;
	DisplayControl* Control;

	ColorPalette*  BGColors;
	SpritePalette* Palette_0;
	SpritePalette* Palette_1;

	std::map<Byte,   Tile*> SpriteTable;
	std::map<int8_t, Tile*> TilePatternTable;

	std::vector<SpriteAttribute*> SpriteAttributes;

	std::map<glm::uvec2, Byte*,   VectorComparator> TileMap_1;
	std::map<glm::uvec2, int8_t*, VectorComparator> TileMap_2;

	Tile* Get_Tile(Byte tileX, Byte tileY);

	void Draw_Line();

	void Draw_Background();
	void Draw_Sprites();
};
