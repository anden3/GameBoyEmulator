#pragma once

#include <string>
#include <iostream>

typedef uint8_t  Byte;
typedef uint16_t Word;

typedef Byte (*ConditionFunc)(void);

class CPU;

class Condition {
public:
    Condition() {}
    Condition(std::string expr, bool &status);

    bool Evaluate() const;

    Byte Operator;
    static CPU* cpu;

    friend std::ostream& operator<<(std::ostream &os, const Condition &c);

private:
	Byte* A_Byte;
    Word* A_Word;

    Byte  B_Byte_Val;
    Word  B_Word_Val;
    Word* B_Ptr;

    bool  A_Is_Bit     = false;
    bool  A_Is_Byte    = true;
    Byte  BitOffset    = 0;

    bool  B_Is_Byte    = true;
    bool  B_Is_Pointer = false;

    std::string A_Str;
    std::string B_Str;

    bool Parse(std::string expr);
    bool Compile();

    ConditionFunc func;
};
