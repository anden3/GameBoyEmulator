#include "Condition.hpp"

#include <asmjit/asmjit.h>

#include "CPU.hpp"
#include "Memory.hpp"
#include "Helpers.hpp"

static asmjit::JitRuntime rt;

enum ConditionalOperators : Byte {
    EQUAL,
    NOT_EQUAL,
    GREATER,
    GREATER_OR_EQUAL,
    LESS,
    LESS_OR_EQUAL,

    OPERATOR_COUNT
};

static const std::map<std::string, Byte> FlagNames = {
    {"Z", 7}, {"S", 6}, {"HC", 5}, {"CC", 4}
};

#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Wweak-vtables"

class AsmJitException : public std::exception {
public:
    AsmJitException(asmjit::Error err, const char* m) noexcept : error(err), message(m) {}

    const char* what() const noexcept override {
        return message.c_str();
    }

    asmjit::Error error;
    std::string message;
};

class ThrowErrorHandler : public asmjit::ErrorHandler {
public:
    bool handleError(asmjit::Error err, const char* message, asmjit::CodeEmitter* origin) override {
        throw AsmJitException(err, message);
    }
};

#pragma clang diagnostic pop

CPU* Condition::cpu = nullptr;

Condition::Condition(std::string expr, bool &status) {
    status = Parse(expr);
}

bool Condition::Parse(std::string expr) {
    expr.erase(std::remove(expr.begin(), expr.end(), ' '), expr.end());

    size_t opPos = expr.find_first_of("!><=");
    size_t secondPos = opPos + 1;

    if (opPos == std::string::npos) {
        std::cerr << "Error! No operators in conditional breakpoint.\n";
        return false;
    }

    char opChar = expr[opPos];

    if (opChar == '!') {
        Operator = NOT_EQUAL;
        secondPos++;
    }
    else if (opChar == '=') {
        Operator = EQUAL;
    }
    else {
        if (expr[opPos + 1] == '=') {
            secondPos++;

            if (opChar == '>') {
                Operator = GREATER_OR_EQUAL;
            }
            else {
                Operator = LESS_OR_EQUAL;
            }
        }
        else {
            if (opChar == '>') {
                Operator = GREATER;
            }
            else {
                Operator = LESS;
            }
        }
    }

    A_Str = expr.substr(0, opPos);
    B_Str = expr.substr(secondPos);

    if (FlagNames.count(A_Str)) {
        A_Is_Bit = true;
        BitOffset = FlagNames.at(A_Str);
        A_Byte = &cpu->registers.f;
    }
    else if (cpu->RegisterByteNames.count(A_Str)) {
        A_Byte = cpu->RegisterByteNames.at(A_Str);
    }
    else if (cpu->RegisterWordNames.count(A_Str)) {
        A_Is_Byte = false;
        A_Word = cpu->RegisterWordNames.at(A_Str);
    }
    else {
        A_Byte = cpu->memory->Get_Byte_Pointer(str_to_num<Word>(A_Str));
    }

    if (B_Str[0] == '&') {
        B_Is_Pointer = true;
        B_Ptr = cpu->memory->Get_Word_Pointer(str_to_num<Word>(B_Str.substr(1)));
    }
    else {
        if (B_Str.length() > 4) {
            B_Is_Byte = false;
            B_Word_Val = str_to_num<Word>(B_Str);
        }
        else {
            B_Byte_Val = str_to_num<Byte>(B_Str);
        }
    }

    if (!Compile()) {
        std::cerr << "Error! Conditional breakpoint compilation failed.\nCondition was '" << expr << "'\n";
        return false;
    }

    return true;
}

bool Condition::Evaluate() const {
    return (func() == 1);
}

bool Condition::Compile() {
    using namespace asmjit;

    CodeHolder code;
    // FileLogger logger(stdout);
    ThrowErrorHandler eh;

    code.init(rt.getCodeInfo());
    // code.setLogger(&logger);
    code.setErrorHandler(&eh);

    try {
        X86Compiler c(&code);

        c.addFunc(FuncSignature0<Byte>());

        Label L_Exit = c.newLabel();

        X86Gp a;
        X86Gp b;
        X86Gp result = c.newGpb();

        if (A_Is_Byte && B_Is_Byte) {
            a = c.newGpb();
            b = c.newGpb();
        }
        else {
            a = c.newGpw();
            b = c.newGpw();
        }

        if (A_Is_Byte) {
            c.mov(a, x86::byte_ptr(reinterpret_cast<std::uintptr_t>(A_Byte)));

            if (A_Is_Bit) {
                c.and_(a, 1 << BitOffset);
                c.ror(a, BitOffset);
            }
        }
        else {
            c.mov(a, x86::word_ptr(reinterpret_cast<std::uintptr_t>(A_Word)));
        }

        if (B_Is_Pointer) {
            c.mov(b, x86::word_ptr(reinterpret_cast<std::uintptr_t>(B_Ptr)));
        }
        else if (B_Is_Byte) {
            c.mov(b, B_Byte_Val);
        }
        else {
            c.mov(b, B_Word_Val);
        }

        c.cmp(a, b);

        switch (Operator) {
            case EQUAL:
                c.je(L_Exit);
                break;

            case NOT_EQUAL:
                c.jne(L_Exit);
                break;

            case GREATER:
                c.jg(L_Exit);
                break;

            case GREATER_OR_EQUAL:
                c.jge(L_Exit);
                break;

            case LESS:
                c.jl(L_Exit);
                break;

            case LESS_OR_EQUAL:
                c.jle(L_Exit);
                break;
        }

        c.mov(result, 0);
        c.ret(result);

        c.bind(L_Exit);
        c.mov(result, 1);
        c.ret(result);

        c.endFunc();
        c.finalize();

        rt.add(&func, &code);
    }
    catch (const AsmJitException &ex) {
        std::cout << "Compilation Error! " << ex.what() << '\n';
        return false;
    }

    return true;
}

std::ostream& operator<<(std::ostream &os, const Condition &c) {
    static const std::string operatorSymbols[OPERATOR_COUNT] = {
        "=", "!=", ">", ">=", "<", "<="
    };

    os << c.A_Str << " " << operatorSymbols[c.Operator] << " " << c.B_Str;
    return os;
}
