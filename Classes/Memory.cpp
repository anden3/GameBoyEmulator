#include "Memory.hpp"

#include <iostream>

#include <boost/iostreams/device/mapped_file.hpp>

#include "CPU.hpp"
#include "Display.hpp"

Memory::Memory() {}

void Memory::Initialize(CPU* c, Display* dp) {
    cpu = c;
    display = dp;
}

void Memory::Reset() {
	RAM.clear();
	RAM.reserve(0x10000);

	ROMUnmappedMemory.clear();
	ROMUnmappedMemory.reserve(0x100);
}

bool Memory::Map_Bootstrap(std::string path) {
	boost::iostreams::mapped_file_source bootstrap(path);

    if (bootstrap.size() > 0x100) {
    	std::cerr << "ERROR! BOOTSTRAP TOO LARGE.\n";
    	return false;
    }

   	RAM.insert(RAM.begin(), bootstrap.begin(), bootstrap.end());
   	return true;
}

void Memory::Unmap_Bootstrap() {
	std::vector<Byte> copy = ROMUnmappedMemory;

	ROMUnmappedMemory.insert(ROMUnmappedMemory.begin(), RAM.begin(), RAM.begin() + 256);
    RAM.insert(RAM.begin(), copy.begin(), copy.end());
}

bool Memory::Load_ROM(std::string path) {
	boost::iostreams::mapped_file_source rom(path);

    if (rom.size() > 0x10000) {
    	std::cerr << "ERROR! ROM TOO LARGE.\n";
    	return false;
    }

    ROMUnmappedMemory.insert(ROMUnmappedMemory.begin(), rom.begin(), rom.begin() + 256);
    RAM.insert(RAM.begin() + 256, rom.begin() + 256, rom.end());
    return true;
}

int8_t Memory::Read_Signed_Byte(Word address) {
	return static_cast<int8_t>(RAM[address]);
}

void Memory::Write_Word(Word address, Word value) {
    RAM[address] = (value & 0xFF);
    RAM[address + 1] = ((value >> 8) & 0xFF);
    LastWrittenTo = address;
}
