RegisterFunctions = {	
	{0xFF46, [this](Byte &val) { display->DMA_Transfer(val); }},

    {0xFF50, [this](Byte &val) {
    	if (val == 1) memory->Unmap_Bootstrap();
    }}
};
