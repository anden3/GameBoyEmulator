#pragma once

// #     means Byte
// ##    means Word.
// (...) means Pointer.

#include <map>

typedef uint8_t  Byte;
typedef uint16_t Word;
typedef std::vector<std::pair<std::string, Byte>> InstructionDesc;

enum TokenTypes : Byte {
    OPCODE,

    REG_BYTE, REG_WORD,
    IMM_BYTE, IMM_WORD,
    SIGNED_IMM_BYTE,

    REG_WORD_PTR, IMM_WORD_PTR,
    END_REG_PTR, END_IMM_PTR
};

const std::map<Byte, InstructionDesc> OpcodeNames = {
    {0x00, {
        {"NOP", OPCODE}
    }},
    {0x01, {
        {"LD", OPCODE}, {"BC", REG_WORD}, {"##", IMM_WORD}
    }},
    {0x02, {
        {"LD", OPCODE}, {"(BC)", REG_WORD_PTR}, {"A", REG_BYTE}
    }},
    {0x03, {
        {"INC", OPCODE}, {"BC", REG_WORD}
    }},
    {0x04, {
        {"INC", OPCODE}, {"B", REG_BYTE}
    }},
    {0x05, {
        {"DEC", OPCODE}, {"B", REG_BYTE}
    }},
    {0x06, {
        {"LD", OPCODE}, {"B", REG_BYTE}, {"#", IMM_BYTE}
    }},
    {0x07, {
        {"RLC", OPCODE}, {"A", REG_BYTE}
    }},
    {0x08, {
        {"LD", OPCODE}, {"(##)", IMM_WORD_PTR}, {"SP", REG_WORD}
    }},
    {0x09, {
        {"ADD", OPCODE}, {"HL", REG_WORD}, {"BC", REG_WORD}
    }},
    {0x0A, {
        {"LD", OPCODE}, {"A", REG_BYTE}, {"(BC)", REG_WORD_PTR}
    }},
    {0x0B, {
        {"DEC", OPCODE}, {"BC", REG_WORD}
    }},
    {0x0C, {
        {"INC", OPCODE}, {"C", REG_BYTE}
    }},
    {0x0D, {
        {"DEC", OPCODE}, {"C", REG_BYTE}
    }},
    {0x0E, {
        {"LD", OPCODE}, {"C", REG_BYTE}, {"#", IMM_BYTE}
    }},

    {0x11, {
        {"LD", OPCODE}, {"DE", REG_WORD}, {"##", IMM_WORD}
    }},

    {0x13, {
        {"INC", OPCODE}, {"DE", REG_WORD}
    }},

    {0x15, {
        {"DEC", OPCODE}, {"D", REG_BYTE}
    }},
    {0x16, {
        {"LD", OPCODE}, {"D", REG_BYTE}, {"#", IMM_BYTE}
    }},
    {0x17, {
        {"RL", OPCODE}, {"A", REG_BYTE}
    }},
    {0x18, {
        {"JR", OPCODE}, {"#", IMM_BYTE}
    }},

    {0x1A, {
        {"LD", OPCODE}, {"A", REG_BYTE}, {"(DE)", REG_WORD_PTR}
    }},

    {0x1D, {
        {"DEC", OPCODE}, {"E", REG_BYTE}
    }},
    {0x1E, {
        {"LD", OPCODE}, {"E", REG_BYTE}, {"#", IMM_BYTE}
    }},
    {0x1F, {
        {"RR", OPCODE}, {"A", REG_BYTE}
    }},
    {0x20, {
        {"JR NZ", OPCODE}, {"#", SIGNED_IMM_BYTE}
    }},
    {0x21, {
        {"LD", OPCODE}, {"HL", REG_WORD}, {"##", IMM_WORD}
    }},
    {0x22, {
        {"LDI", OPCODE}, {"(HL)", REG_WORD_PTR}, {"A", REG_BYTE}
    }},
    {0x23, {
        {"INC", OPCODE}, {"HL", REG_WORD}
    }},
    {0x24, {
        {"INC", OPCODE}, {"H", REG_BYTE}
    }},

    {0x28, {
        {"JR Z", OPCODE}, {"#", IMM_BYTE}
    }},

    {0x2E, {
        {"LD", OPCODE}, {"L", REG_BYTE}, {"#", IMM_BYTE}
    }},

    {0x31, {
        {"LD", OPCODE}, {"SP", REG_WORD}, {"##", IMM_WORD}
    }},
    {0x32, {
        {"LDD", OPCODE}, {"(HL)", REG_WORD_PTR}, {"A", REG_BYTE}
    }},

    {0x3D, {
        {"DEC", OPCODE}, {"A", REG_BYTE}
    }},
    {0x3E, {
        {"LD", OPCODE}, {"A", REG_BYTE}, {"#", IMM_BYTE}
    }},

    {0x4F, {
        {"LD", OPCODE}, {"C", REG_BYTE}, {"A", REG_BYTE}
    }},

    {0x57, {
        {"LD", OPCODE}, {"D", REG_BYTE}, {"A", REG_BYTE}
    }},

    {0x67, {
        {"LD", OPCODE}, {"H", REG_BYTE}, {"A", REG_BYTE}
    }},

    {0x77, {
        {"LD", OPCODE}, {"(HL)", REG_WORD_PTR}, {"A", REG_BYTE}
    }},
    {0x78, {
        {"LD", OPCODE}, {"A", REG_BYTE}, {"B", REG_BYTE}
    }},

    {0x7B, {
        {"LD", OPCODE}, {"A", REG_BYTE}, {"E", REG_BYTE}
    }},
    {0x7C, {
        {"LD", OPCODE}, {"A", REG_BYTE}, {"H", REG_BYTE}
    }},
    {0x7D, {
        {"LD", OPCODE}, {"A", REG_BYTE}, {"L", REG_BYTE}
    }},

    {0x86, {
        {"ADD", OPCODE}, {"A", REG_BYTE}, {"(HL)", REG_WORD_PTR}
    }},

    {0x90, {
        {"SUB", OPCODE}, {"B", REG_BYTE}
    }},

    {0x99, {
        {"SBC", OPCODE}, {"A", REG_BYTE}, {"C", REG_BYTE}
    }},

    {0xAF, {
        {"XOR", OPCODE}, {"A", REG_BYTE}
    }},

    {0xBE, {
        {"CP", OPCODE}, {"(HL)", REG_WORD_PTR}
    }},

    {0xC1, {
        {"POP", OPCODE}, {"BC", REG_WORD}
    }},

    {0xC3, {
        {"JP", OPCODE}, {"##", IMM_WORD}
    }},

    {0xC5, {
        {"PUSH", OPCODE}, {"BC", REG_WORD}
    }},

    {0xC9, {
        {"RET", OPCODE}
    }},

    {0xCD, {
        {"CALL", OPCODE}, {"##", IMM_WORD}
    }},

    {0xE0, {
        {"LD", OPCODE}, {"(0xFF00+#)", END_IMM_PTR}, {"A", REG_BYTE}
    }},

    {0xE2, {
        {"LD", OPCODE}, {"C", END_REG_PTR}, {"A", REG_BYTE}
    }},

    {0xEA, {
        {"LD", OPCODE}, {"(##)", IMM_WORD_PTR}, {"A", REG_BYTE}
    }},

    {0xF0, {
        {"LD", OPCODE}, {"A", REG_BYTE}, {"(0xFF00+#)", END_IMM_PTR}
    }},

    {0xF3, {
        {"DI", OPCODE}
    }},

    {0xFB, {
        {"EI", OPCODE}
    }},

    {0xFE, {
        {"CP", OPCODE}, {"#", IMM_BYTE}
    }}
};

const std::map<Byte, InstructionDesc> CGBOpcodeNames = {
    {0x11, {
        {"RL", OPCODE}, {"C", REG_BYTE}
    }},
    {0x7C, {
        {"BIT 7", OPCODE}, {"H", REG_BYTE}
    }}
};
