CGBInstructions = {
	// RL C
	{0x11, [this]() {
		registers.flagSub = false;
		registers.flagHalf = false;

		bool oldCarry = registers.flagCarry;
		registers.flagCarry = registers.c & 0x80;
		rotate_left(registers.c);

		if (oldCarry) {
            registers.c |= 0x1;
        }
        else {
            registers.c &= ~(0x1);
        }

        registers.flagZero = (registers.c == 0x0);
        return 2;
	}},

	// BIT 7, H
	{0x7C, [this]() {
		registers.flagZero = !(registers.h & 0x80);
		registers.flagSub = false;
		registers.flagHalf = true;
		return 2;
	}}
};
