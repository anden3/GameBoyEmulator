Instructions = {
    // NOP
    {0x00, [this] {
        return 1;
    }},

    // LD BC, nn
    {0x01, [this] {
        registers.bc = memory->Read_Word(registers.pc);
        registers.pc += 2;
        return 3;
    }},

    // LD (BC), A
    {0x02, [this] {
        memory->Write_Byte(registers.bc, registers.a);
        return 2;
    }},

    // INC BC
    {0x03, [this] {
        if (registers.bc < 0xFFFF) {
            ++registers.b;
        }

        return 2;
    }},

    // INC B
    {0x04, [this] {
        Add(registers.b, 1, false);
        return 1;
    }},

    // DEC B
    {0x05, [this] {
        Add(registers.b, -1, false);
        return 1;
    }},

    // LD B, n
    {0x06, [this] {
        registers.b = memory->Read_Byte(registers.pc++);
        return 2;
    }},

    // RLC A
    {0x07, [this] {
        registers.f = 0;
        registers.flagCarry = registers.a & 0x80;
        rotate_left(registers.a);
        return 1;
    }},

    // LD (nn), SP
    {0x08, [this] {
        Word addr = memory->Read_Word(registers.pc);
        memory->Write_Word(addr, registers.sp);
        registers.pc += 2;
        return 5;
    }},

    // ADD HL, BC
    {0x09, [this] {
        Add(registers.hl, registers.bc);
        return 2;
    }},

    // LD A, (BC)
    {0x0A, [this] {
        registers.a = memory->Read_Byte(registers.bc);
        return 2;
    }},

    // DEC BC
    {0x0B, [this] {
        registers.bc -= 1;
        return 2;
    }},

    // INC C
    {0x0C, [this] {
        Add(registers.c, 1, false);
        return 1;
    }},

    // DEC C
    {0x0D, [this] {
        Add(registers.c, -1, false);
        return 1;
    }},

    // LD C,n
    {0x0E, [this] {
        registers.c = memory->Read_Byte(registers.pc++);
        return 2;
    }},

    // LD DE, nn
    {0x11, [this] {
        registers.de = memory->Read_Word(registers.pc);
        registers.pc += 2;
        return 3;
    }},

    // INC DE
    {0x13, [this] {
        registers.de += 1;
        return 2;
    }},

    // DEC D
    {0x15, [this] {
        Add(registers.d, -1, false);
        return 1;
    }},

    // LD D, n
    {0x16, [this] {
        registers.d = memory->Read_Byte(registers.pc++);
        return 2;
    }},

    // RLA
    {0x17, [this] {
        registers.flagSub = false;
        registers.flagHalf = false;

        bool oldCarry = registers.flagCarry;
        registers.flagCarry = registers.a & 0x80;
        rotate_left(registers.a);

        if (oldCarry) {
            registers.a |= 0x1;
        }
        else {
            registers.a &= ~(0x1);
        }

        registers.flagZero = (registers.a == 0x0);
        return 1;
    }},

    // JR n
    {0x18, [this] {
        registers.pc += memory->Read_Signed_Byte(registers.pc++);
        return 2;
    }},

    // LD A, (DE)
    {0x1A, [this] {
        registers.a = memory->Read_Byte(registers.de);
        return 2;
    }},

    // DEC E
    {0x1D, [this] {
        Add(registers.e, -1, false);
        return 1;
    }},

    // LD E, n
    {0x1E, [this] {
        registers.e = memory->Read_Byte(registers.pc++);
        return 2;
    }},

    // RRA
    {0x1F, [this] {
        registers.f = 0;

        bool oldCarry = registers.flagCarry;
        registers.flagCarry = registers.a & 0x1;
        rotate_right(registers.a);

        if (oldCarry) {
            registers.a |= 0x80;
        }
        else {
            registers.a &= ~(0x80);
        }

        return 1;
    }},

    // JR NZ, n
    {0x20, [this] {
        char offset = memory->Read_Signed_Byte(registers.pc++);

        if (!registers.flagZero) {
            registers.pc += offset;
            return 3;
        }

        return 2;
    }},

    // LD HL, nn
    {0x21, [this] {
        registers.hl = memory->Read_Word(registers.pc);
        registers.pc += 2;
        return 2;
    }},

    // LDI (HL), A
    {0x22, [this] {
        memory->Write_Byte(registers.hl++, registers.a);
        return 2;
    }},

    // INC HL
    {0x23, [this] {
        registers.hl += 1;
        return 2;
    }},

    // INC H
    {0x24, [this] {
        Add(registers.h, 1, false);
        return 1;
    }},

    // JR Z, n
    {0x28, [this] {
        char offset = memory->Read_Signed_Byte(registers.pc++);

        if (registers.flagZero) {
            registers.pc += offset;
            return 3;
        }

        return 2;
    }},

    // LD L, n
    {0x2E, [this] {
        registers.l = memory->Read_Byte(registers.pc++);
        return 2;
    }},

    // LD SP, nn
    {0x31, [this] {
        registers.sp = memory->Read_Word(registers.pc);
        registers.pc += 2;
        return 3;
    }},

    // LDD (HL), A
    {0x32, [this] {
        memory->Write_Byte(registers.hl--, registers.a);
        return 2;
    }},

    // DEC A
    {0x3D, [this] {
        Add(registers.a, -1, false);
        return 1;
    }},

    // LD A, #
    {0x3E, [this] {
        registers.a = memory->Read_Byte(registers.pc++);
        return 2;
    }},

    // LD C, A
    {0x4F, [this] {
        registers.c = registers.a;
        return 1;
    }},

    // LD D, A
    {0x57, [this] {
        registers.d = registers.a;
        return 1;
    }},

    // LD H, A
    {0x67, [this] {
        registers.h = registers.a;
        return 1;
    }},

    // LD (HL), A
    {0x77, [this] {
        memory->Write_Byte(registers.hl, registers.a);
        return 2;
    }},

    // LD A, B
    {0x78, [this] {
        registers.a = registers.b;
        return 1;
    }},

    // LD A, E
    {0x7B, [this] {
        registers.a = registers.e;
        return 1;
    }},

    // LD A, H
    {0x7C, [this] {
        registers.a = registers.h;
        return 1;
    }},

    // LD A, L
    {0x7D, [this] {
        registers.a = registers.l;
        return 1;
    }},

    // ADD A, (HL)
    {0x86, [this] {
        Add(registers.a, memory->Read_Byte(registers.hl));
        return 2;
    }},

    // SUB B
    {0x90, [this] {
        Add(registers.a, -registers.b);
        return 1;
    }},

    // SBC A, C
    {0x99, [this] {
        Add(registers.a, -(registers.c + registers.flagCarry));
        return 1;
    }},

    // XOR A
    {0xAF, [this] {
        registers.a = 0;
        registers.f = 0x80;
        return 1;
    }},

    // CP (HL)
    {0xBE, [this] {
        Compare(registers.a, memory->Read_Byte(registers.hl));
        return 2;
    }},

    // POP BC
    {0xC1, [this] {
        registers.sp += 2;
        registers.bc = memory->Read_Word(registers.sp);
        return 3;
    }},

    // JP nn
    {0xC3, [this] {
        Word destination = memory->Read_Word(registers.pc);
        registers.pc = destination;
        return 4;
    }},

    // PUSH BC
    {0xC5, [this] {
        memory->Write_Word(registers.sp, registers.bc);
        registers.sp -= 2;
        return 4;
    }},

    // RET
    {0xC9, [this] {
        registers.sp += 2;
        registers.pc = memory->Read_Word(registers.sp);
        return 2;
    }},

    // CALL nn
    {0xCD, [this] {
        memory->Write_Word(registers.sp, registers.pc + 2);
        registers.sp -= 2;
        registers.pc = memory->Read_Word(registers.pc);
        return 3;
    }},

    // LDH (n), A
    {0xE0, [this] {
        Byte offset = memory->Read_Byte(registers.pc++);
        memory->Write_Byte(0xFF00 + offset, registers.a);
        return 3;
    }},

    // LD (C), A
    {0xE2, [this] {
        memory->Write_Byte(0xFF00 + registers.c, registers.a);
        return 2;
    }},

    // LD (nn), A
    {0xEA, [this] {
        Word addr = memory->Read_Word(registers.pc);
        memory->Write_Byte(addr, registers.a);
        registers.pc += 2;
        return 4;
    }},

    // LDH A, (n)
    {0xF0, [this] {
        Byte offset = memory->Read_Byte(registers.pc++);
        registers.a = memory->Read_Byte(0xFF00 + offset);
        return 3;
    }},

    // DI
    {0xF3, [this] {
        if (AllowInterrupts) {
            ToggleInterrupts = true;
        }

        return 1;
    }},

    // EI
    {0xFB, [this] {
        if (!AllowInterrupts) {
            ToggleInterrupts = true;
        }

        return 1;
    }},

    // CP #
    {0xFE, [this] {
        Compare(registers.a, memory->Read_Byte(registers.pc++));
        return 2;
    }}
};
