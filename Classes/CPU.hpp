#pragma once

#include <map>
#include <bitset>
#include <vector>
#include <cstdint>
#include <functional>

#include "Condition.hpp"

const bool DEBUG_MODE = true;

typedef uint8_t  Byte;
typedef uint16_t Word;

class Memory;
class Display;

class CPU {
public:
    CPU();

    struct clock {
        Word m;
        Word t;
    } clock;

    struct registers {
        struct {
            union {
                struct {
                    union {
                        struct {
                            bool : 4;

                            bool flagCarry : 1;
                            bool flagHalf  : 1;
                            bool flagSub   : 1;
                            bool flagZero  : 1;
                        };
                        Byte f;
                    };

                    Byte a;
                };

                Word af;
            };
        };
        
        struct {
            union {
                struct {
                    Byte c;
                    Byte b;
                };
                Word bc;
            };
        };
        
        struct {
            union {
                struct {
                    Byte e;
                    Byte d;
                };
                Word de;
            };
        };
        
        struct {
            union {
                struct {
                    Byte l;
                    Byte h;
                };
                Word hl;
            };
        };
        
        Word sp;
        Word pc;
    } registers;

    void Initialize(Memory* mem, Display* dp);
    void Reset();
    void Dispatch_Loop();

    void Signal_Handler(int signal);

    friend class Condition;

private:
    // Components
    Memory* memory;
    Display* display;

    // Interrupts
    bool ToggleInterrupts = false;
    bool AllowInterrupts  = false;

    bool Exit = false;

    Word CyclesUntilNextInstruction = 0;

    // Debugging
    bool Logging  = false;
    bool Stepping = true;
    Word RunTo = 0x0000;
    std::string LastDebugInstruction = "";

    std::vector<Word> Breakpoints;

    std::vector<std::tuple<Word*, Word, Byte>> TrackedValues;
    std::vector<std::vector<Condition>> CondBreakpoints;

    std::map<Byte, std::function<Byte(void)>> Instructions;
    std::map<Byte, std::function<Byte(void)>> CGBInstructions;

    std::map<Word, std::function<void(Byte&)>> RegisterFunctions;

    const std::map<std::string, Byte*> RegisterByteNames = {
        {"A", &registers.a}, {"B", &registers.b},
        {"C", &registers.c}, {"D", &registers.d},
        {"E", &registers.e}, {"F", &registers.f},
        {"H", &registers.h}, {"L", &registers.l}
    };

    const std::map<std::string, Word*> RegisterWordNames = {
        {"AF", &registers.af}, {"BC", &registers.bc},
        {"DE", &registers.de}, {"HL", &registers.hl},
        {"SP", &registers.sp}, {"PC", &registers.pc}
    };

    Byte        Run_Instruction();
    std::string Decode_Instruction(Byte opcode, Byte extraOpcode = 0x00);

    void Poll_Registers();

    bool Check_Debug_Stop();
    int  Process_Debug();

    void Compare(Byte a, Byte b);

    void Check_Overflow(Byte value, bool subtraction = false);
    void Check_Overflow(Word value, bool subtraction = false);

    void Add(Byte &a, int8_t b, bool checkCarry = true);
    void Add(Word &a, int16_t b, bool checkCarry = true);
};
