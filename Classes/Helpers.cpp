#include "Helpers.hpp"

typedef uint8_t  Byte;
typedef uint16_t Word;

std::vector<std::string> split(const std::string &s, char delim) {
    std::vector<std::string> elems;

    std::stringstream ss(s);
    std::string item;

    while (std::getline(ss, item, delim)) {
        elems.push_back(item);
    }

    return elems;
}

std::string join(const std::vector<std::string> &vec, const char* delim) {
    std::stringstream ss;
    std::copy(vec.begin(), vec.end(), std::ostream_iterator<std::string>(ss, delim));
    std::string s = ss.str();

    return s.substr(0, s.size() - 1);
}

template <>
std::string to_hex<Byte>(Byte b, bool prefix) {
    static const char* lookup = "0123456789ABCDEF";

    std::string result;

    if (prefix) {
        result += "0x";
    }

    result += lookup[(b >> 4) & 0xF];
    result += lookup[b & 0xF];

    return result;
}

template <>
std::string to_hex<Word>(Word w, bool prefix) {
    static const char* lookup = "0123456789ABCDEF";

    std::string result;

    if (prefix) {
        result += "0x";
    }

    result += lookup[(w >> 12) & 0xF];
    result += lookup[(w >> 8)  & 0xF];
    result += lookup[(w >> 4)  & 0xF];
    result += lookup[w & 0xF];

    return result;
}
