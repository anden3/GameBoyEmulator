#pragma once

#include <vector>
#include <string>
#include <cstdint>

typedef uint8_t  Byte;
typedef uint16_t Word;

class CPU;
class Display;

class Memory {
public:
   	Memory();

   	void Initialize(CPU* c, Display* dp);
   	void Reset();

    bool Load_ROM(std::string path);

    bool Map_Bootstrap(std::string path);
    void Unmap_Bootstrap();

    inline Byte Read_Byte(Word address) {
        return RAM[address];
    }
    int8_t Read_Signed_Byte(Word address);

    inline Word Read_Word(Word address) {
        return static_cast<Word>(RAM[address + 1] << 8) | RAM[address];
    }

    inline void Write_Byte(Word address, Byte value) {
        RAM[address] = value;
        LastWrittenTo = address;
    }
    void Write_Word(Word address, Word value);

    inline Byte* Get_Byte_Pointer(Word address) {
        return &RAM[address];
    }

    inline Word* Get_Word_Pointer(Word address) {
        return reinterpret_cast<Word*>(&RAM[address]);
    }

    Word LastWrittenTo = 0x0000;

private:
    CPU* cpu;
    Display* display;

    std::vector<Byte> RAM;
    std::vector<Byte> ROMUnmappedMemory;
};
