#include "Display.hpp"

#include <iostream>

#include <GL/glew.h>
#include <GLFW/glfw3.h>

#include "CPU.hpp"
#include "Buffer.hpp"
#include "Memory.hpp"
#include "Shader.hpp"
#include "Helpers.hpp"

static const Word CYCLES_PER_LINE = 456;

static GLFWwindow* Window = nullptr;
static Shader* shader = nullptr;

static Buffer buffer;

struct ColorPalette {
	union {
		struct {
			Byte Color_0 : 2;
			Byte Color_1 : 2;
			Byte Color_2 : 2;
			Byte Color_3 : 2;
		};

		Byte AllColors;
	};
};

struct SpritePalette {
	union {
		struct {
			Byte : 2;
			Byte Color_1 : 2;
			Byte Color_2 : 2;
			Byte Color_3 : 2;
		};

		Byte AllColors;
	};
};

struct SpriteAttribute {
	Byte YPos;
	Byte XPos;
	Byte TileNumber;

	union {
		struct {
			bool : 4;
			bool PaletteNum : 1;
			bool XFlipped   : 1;
			bool YFlipped   : 1;

			// 0 = OBJ Above BG, 1 = OBJ Behind BG color 1-3
			bool Obj_BG_Priority : 1;
		};
		Byte Flags;
	};
};

struct DisplayControl {
	bool BGDisplay     : 1;
	bool SpriteDisplay : 1;
	bool SpriteSize    : 1;
	bool BGTileMap     : 1;
	bool TileData      : 1;
	bool WindowDisplay : 1;
	bool WindowTilemap : 1;
	bool LCDEnable     : 1;
};

struct DisplayStatus {
	Byte Mode      : 2;
	bool LYEqFlag  : 1;
	bool HBlankInt : 1;
	bool VBlankInt : 1;
	bool OAMInt    : 1;
	bool LYEqInt   : 1;
	bool           : 1;
};

struct Tile {
	Byte Get_Color(Byte x, Byte y) {
		Byte ls = static_cast<Byte>(Rows[y] & 0x00FF);
		Byte ms = static_cast<Byte>(Rows[y] & 0xFF00);

		bool lsb = ls & (1 << x);
		bool msb = ms & (1 << x);

		return msb * 2 + lsb;
	}

	Word Rows[8];
};

enum ColorValues : Byte {
	WHITE,
	LIGHT_GREY,
	DARK_GREY,
	BLACK
};

void Init_GL() {
	glfwInit();

    // Set the OpenGL version.
    glfwWindowHint(GLFW_CONTEXT_VERSION_MAJOR, 4);
    glfwWindowHint(GLFW_CONTEXT_VERSION_MINOR, 1);
    glfwWindowHint(GLFW_OPENGL_FORWARD_COMPAT, true);
    glfwWindowHint(GLFW_OPENGL_PROFILE, GLFW_OPENGL_CORE_PROFILE);

    glfwWindowHint(GLFW_RESIZABLE, false);
	glfwWindowHint(GLFW_AUTO_ICONIFY, false);

    glfwWindowHint(GLFW_DECORATED, true);
    Window = glfwCreateWindow(160, 144, "GameBoyEmulator", nullptr, nullptr);

    glfwSetWindowPos(Window, 0, 0);
    glfwMakeContextCurrent(Window);
    glfwSwapInterval(true);

    glewExperimental = GL_TRUE;
    glewInit();

    glEnable(GL_BLEND);
    glEnable(GL_CULL_FACE);
    glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);
    glViewport(0, 0, 160, 144);

    glClearColor(1.0f, 1.0f, 1.0f, 1.0f);
}

Display::Display() {}

void Display::Initialize(CPU* c, Memory* mem) {
	Init_GL();

	shader = new Shader("shader");

	buffer.Init(shader);
    buffer.Create(1);
    buffer.VertexType = GL_POINTS;

	cpu = c;
	memory = mem;

	Control = reinterpret_cast<DisplayControl*>(memory->Get_Byte_Pointer(0xFF40));
	Status  = reinterpret_cast<DisplayStatus*> (memory->Get_Byte_Pointer(0xFF41));

	ScrollY = memory->Get_Byte_Pointer(0xFF42);
	ScrollX = memory->Get_Byte_Pointer(0xFF43);

	WindowY = memory->Get_Byte_Pointer(0xFF4A);
	WindowX = memory->Get_Byte_Pointer(0xFF4B);

	LY = memory->Get_Byte_Pointer(0xFF44);

	BGColors  = reinterpret_cast<ColorPalette*> (memory->Get_Byte_Pointer(0xFF47));
	Palette_0 = reinterpret_cast<SpritePalette*>(memory->Get_Byte_Pointer(0xFF48));
	Palette_1 = reinterpret_cast<SpritePalette*>(memory->Get_Byte_Pointer(0xFF49));

	for (Byte i = 0; i < 255; ++i) {
        SpriteTable[i]            = reinterpret_cast<Tile*>(memory->Get_Byte_Pointer(0x8000 + (i * 16)));
        TilePatternTable[i - 128] = reinterpret_cast<Tile*>(memory->Get_Byte_Pointer(0x8800 + (i * 16)));
    }

    for (Word i = 0; i < 0x400; ++i) {
    	Byte x = i % 32;
    	Byte y = static_cast<Byte>(i / 32);

    	TileMap_1[glm::uvec2(x, y)] = memory->Get_Byte_Pointer(0x9800 + i);
    	TileMap_2[glm::uvec2(x, y)] = reinterpret_cast<int8_t*>(memory->Get_Byte_Pointer(0x9C00 + i));
    }
}

void Display::Process() {
	if (!Control->LCDEnable) {
		return;
	}

	glfwPollEvents();

	CurrentLineCycles = cpu->clock.m - CurrentLineStart;

	if (CurrentLineCycles >= CYCLES_PER_LINE) {
		CurrentLineCycles %= CYCLES_PER_LINE;
		CurrentLineStart = cpu->clock.m - CurrentLineCycles;

		*LY += 1;

		if (*LY == 144) {
			VBlank = true;
		}
		else if (*LY == 153) {
			*LY = 0;
			VBlank = false;
		}

		if (!VBlank) {
			Draw_Line();
		}
	}
}

void Display::Reset() {
	VBlank = false;
	CurrentLineStart = 0;
	CurrentLineCycles = 0;
}

Tile* Display::Get_Tile(Byte tileX, Byte tileY) {
	short tileNum;

	if (Control->WindowTilemap) {
		tileNum = *TileMap_2[{tileX, tileY}];
	}
	else {
		tileNum = *TileMap_1[{tileX, tileY}];
	}

	if (Control->TileData) {
		return SpriteTable[static_cast<Byte>(tileNum)];
	}
	else {
		return TilePatternTable[static_cast<int8_t>(tileNum)];
	}
}

void Display::Draw_Line() {
	if (Control->BGDisplay) {
		Draw_Background();
	}

	if (Control->SpriteDisplay) {
		Draw_Sprites();
	}
}

void Display::Draw_Background() {
	std::vector<int> lineBuffer;
	lineBuffer.reserve(160);

	Byte yPos = 0;
	bool drawWindow = (Control->WindowDisplay) && (*WindowY <= *LY);

	if (!drawWindow) {
		yPos = *ScrollY + *LY;
	}
	else {
		yPos = *LY - *WindowY;
	}

	for (Byte p = 0; p < 160; ++p) {
		Byte xPos = *ScrollX + p;

		if (drawWindow && (p >= *WindowX)) {
			xPos = p - *WindowX;
		}

		Tile* tile = Get_Tile(xPos / 8, yPos / 8);
		lineBuffer[p] = tile->Get_Color(xPos % 8, yPos % 8);
	}

	buffer.Upload(lineBuffer);
	buffer.Draw();

	glfwSwapBuffers(Window);
}

void Display::Draw_Sprites() {

}

void Display::DMA_Transfer(Byte source) {
	Word addr = static_cast<Word>(source << 8);

	for (Word i = 0; i < 0xA0; ++i) {
		memory->Write_Byte(0xFE00 + i, memory->Read_Byte(addr + i));
	}
}
