#include "CPU.hpp"

#include <cstdio>
#include <csignal>
#include <sstream>
#include <algorithm>
#include <stdexcept>

#include <boost/format.hpp>
#include <boost/algorithm/string.hpp>

#include "Memory.hpp"
#include "Display.hpp"
#include "Helpers.hpp"

#include "Data/OpcodeNames.hpp"

enum ExitValues {
    KEEP_LOOPING,
    CONTINUE,
    EXIT,
    RESET
};

enum TrackedValueType {
    RAM_BYTE,
    REGISTER_BYTE,
    REGISTER_WORD
};

CPU::CPU() {
    Condition::cpu = this;
}

void CPU::Initialize(Memory* mem, Display* dp) {
    memory = mem;
    display = dp;

    #include "Data/BaseInstructions.hpp"
    #include "Data/CGBInstructions.hpp"
    #include "Data/RegisterFunctions.hpp"

    Reset();
}

void CPU::Reset() {
    clock.m = 0;
    clock.t = 0;

    registers.a = 0;
    registers.b = 0;
    registers.c = 0;
    registers.d = 0;
    registers.e = 0;
    registers.f = 0;

    registers.h = 0;
    registers.l = 0;

    registers.sp = 0;
    registers.pc = 0x0;

    AllowInterrupts = true;
    ToggleInterrupts = false;

    LastDebugInstruction = "";
    RunTo = 0x0000;

    display->Reset();
    memory->Reset();
}

void CPU::Signal_Handler(int signal) {
    switch (signal) {
        case SIGINT:
            if (Stepping) {
                exit(0);
            }

            Stepping = true;
            break;
    }
}

Byte CPU::Run_Instruction() {
    if (ToggleInterrupts) {
        AllowInterrupts = !AllowInterrupts;
        ToggleInterrupts = false;
    }

    Byte opcodeSize = 1;
    Byte opcode = memory->Read_Byte(registers.pc);
    Byte extraOpcode = 0x00;

    std::function<Byte(void)>* func;

    if (Instructions.count(opcode) == 0) {
        if (opcode == 0xCB) {
            opcodeSize = 2;
            extraOpcode = memory->Read_Byte(registers.pc + 1);

            if (!CGBInstructions.count(extraOpcode)) {
                printf("\nOpcode: CB %.2X has no instruction.\n", extraOpcode);
                return EXIT;
            }

            func = &CGBInstructions[extraOpcode];
        }
        else {
            printf("\nOpcode: %.2X has no instruction.\n", opcode);
            return EXIT;
        }
    }
    else {
        func = &Instructions[opcode];
    }

    if (DEBUG_MODE) {
        if (Check_Debug_Stop()) {
            Logging = false;
            Stepping = true;
            std::cout << to_hex(static_cast<Word>(registers.pc)) << ": " << Decode_Instruction(opcode, extraOpcode) << '\n';

            bool looping = true;

            while (looping) {
                switch (Process_Debug()) {
                    case KEEP_LOOPING:
                        break;

                    case CONTINUE:
                        looping = false;
                        break;

                    case EXIT:
                        return EXIT;

                    case RESET:
                        return RESET;
                }
            }
        }
        else if (Logging) {
            std::cout << to_hex(static_cast<Word>(registers.pc)) << ": " << Decode_Instruction(opcode, extraOpcode) << '\n';
        }
    }

    registers.pc += opcodeSize;

    Word time = (*func)();
    CyclesUntilNextInstruction = time;

    return CONTINUE;
}

std::string CPU::Decode_Instruction(Byte opcode, Byte extraOpcode) {
    Word tempPC = registers.pc + 1;
    InstructionDesc instruction;

    if (opcode == 0xCB) {
        instruction = CGBOpcodeNames.at(extraOpcode);
        tempPC += 1;
    }
    else {
        instruction = OpcodeNames.at(opcode);
    }

    if (instruction.size() == 1) {
        return instruction[0].first;
    }

    std::string result = "";

    for (auto const &p : instruction) {
        switch (p.second) {
            case OPCODE:
                result += p.first + " ";
                continue;

            case REG_BYTE:
            case REG_WORD:
            case REG_WORD_PTR:
                result += p.first;
                break;

            case IMM_BYTE:
                result += to_hex(memory->Read_Byte(tempPC++));
                break;

            case IMM_WORD:
                result += to_hex(memory->Read_Word(tempPC));
                tempPC += 2;
                break;

            case SIGNED_IMM_BYTE:
                result += std::to_string(memory->Read_Signed_Byte(tempPC++));
                break;

            case IMM_WORD_PTR:
                result += "(" + to_hex(memory->Read_Word(tempPC)) + ")";
                tempPC += 2;
                break;

            case END_REG_PTR:
                result += "(" + to_hex(static_cast<Word>(0xFF00 + *RegisterByteNames.at(p.first))) + ")";
                break;

            case END_IMM_PTR:
                result += "(" + to_hex(static_cast<Word>(0xFF00 + memory->Read_Byte(tempPC++))) + ")";
                break;
        }

        result += ", ";
    }

    return result.substr(0, result.size() - 2);
}

bool CPU::Check_Debug_Stop() {
    if (Stepping) {
        return true;
    }

    if (registers.pc == RunTo) {
        RunTo = 0x0000;
        return true;
    }

    if (contains(Breakpoints, registers.pc)) {
        return true;
    }

    for (auto &value : TrackedValues) {
        Word newValue = *std::get<0>(value);
        Word oldValue = std::get<1>(value);

        if (std::get<2>(value) != REGISTER_WORD) {
            newValue &= 0xFF;
        }

        if (newValue != oldValue) {
            std::get<1>(value) = newValue;
            return true;
        }
    }

    for (auto const &cb : CondBreakpoints) {
        bool allPassed = true;

        for (auto const &expr : cb) {
            if (!expr.Evaluate()) {
                allPassed = false;
                break;
            }
        }

        if (allPassed) {
            return true;
        }
    }

    return false;
}

int CPU::Process_Debug() {
    std::cout << "Debugger:> ";

    std::string instruction;
    std::getline(std::cin, instruction);

    if (instruction == "") {
        if (LastDebugInstruction != "") {
            instruction = LastDebugInstruction;
        }
        else {
            return KEEP_LOOPING;
        }
    }

    LastDebugInstruction = instruction;

    auto instructionParts = split(instruction);
    std::string primary = instructionParts[0];

    if (primary == "s" || primary == "step") {
        Stepping = true;
        return CONTINUE;
    }

    else if (primary == "b" || primary == "breakpoint") {
        if (instructionParts.size() == 1) {
            std::cout << "Breakpoints:\n";

            for (Word const &b : Breakpoints) {
                static int i = 0;
                std::cout << '\t' << (i++) << ": " << to_hex(b) << '\n';
            }

            return KEEP_LOOPING;
        }

        if (instructionParts[1] == "del" || instructionParts[1] == "delete") {
            if (instructionParts.size() == 2) {
                std::cerr << "Error! Missing index for deletion.\n";
                return KEEP_LOOPING;
            }

            size_t index = str_to_num<size_t>(instructionParts[2]);

            if (index >= Breakpoints.size()) {
                std::cerr << "Error! Index not in range.\n";
                return KEEP_LOOPING;
            }

            Breakpoints.erase(Breakpoints.begin() + index);
            std::cout << "Breakpoint " << index << " has been deleted.\n";
        }

        else {
            Breakpoints.push_back(str_to_num<Word>(instructionParts[1]));
            std::cout << "Breakpoint " << (Breakpoints.size() - 1) << " has been added!\n";
        }
    }

    else if (primary == "cb") {
        if (instructionParts.size() == 1) {
            std::cout << "Conditional Breakpoints:\n";

            for (auto const &cb : CondBreakpoints) {
                static int i = 0;

                std::cout << "\t#" << (i++) << "\n";

                for (auto const &expr : cb) {
                    std::cout << "\t\t" << expr << '\n';
                }
            }

            return KEEP_LOOPING;
        }

        if (instructionParts[1] == "del" || instructionParts[1] == "delete") {
            if (instructionParts.size() == 2) {
                std::cerr << "Error! Missing index for deletion.\n";
                return KEEP_LOOPING;
            }

            size_t index = str_to_num<size_t>(instructionParts[2]);

            if (index >= CondBreakpoints.size()) {
                std::cerr << "Error! Index not in range.\n";
            }

            CondBreakpoints.erase(CondBreakpoints.begin() + index);
            std::cout << "Conditional Breakpoint " << index << " has been deleted.\n";

            return KEEP_LOOPING;
        }

        std::string exprStr = join(std::vector<std::string>(instructionParts.begin() + 1, instructionParts.end()));
        auto expressions = split(exprStr, '|');

        std::vector<Condition> cb;

        for (auto const &expr : expressions) {
            bool status;
            cb.emplace_back(expr, status);

            if (!status) {
                return KEEP_LOOPING;
            }
        }

        CondBreakpoints.push_back(cb);
        std::cout << "Conditional Breakpoint " << (CondBreakpoints.size() - 1) << " has been added!\n";
    }

    else if (primary == "r" || primary == "run") {
        Logging  = false;
        Stepping = false;
        return CONTINUE;
    }

    else if (primary == "rl" || primary == "run_log") {
        Logging  = true;
        Stepping = false;
        return CONTINUE;
    }

    else if (primary == "track") {
        if (instructionParts.size() == 1) {
            std::cerr << "Error! Not enough arguments.\n";
            return KEEP_LOOPING;
        }

        std::string trackedValue;

        Word* ptr = nullptr;
        Byte type;

        bool deleting = false;

        if (instructionParts.size() > 2) {
            if (instructionParts[1] == "del") {
                deleting = true;
                trackedValue = instructionParts[2];
            }
            else {
                std::cerr << "Error! Too many arguments.\n";
                return KEEP_LOOPING;
            }
        }
        else {
            trackedValue = instructionParts[1];
        }

        std::string trackedValueUpper = boost::to_upper_copy<std::string>(trackedValue);

        if (RegisterWordNames.count(trackedValueUpper)) {
            trackedValue = trackedValueUpper;
            type = REGISTER_WORD;
            ptr = RegisterWordNames.at(trackedValue);
        }
        else if (RegisterByteNames.count(trackedValueUpper)) {
            trackedValue = trackedValueUpper;
            type = REGISTER_BYTE;
            ptr = reinterpret_cast<Word*>(RegisterByteNames.at(trackedValue));
        }
        else {
            type = RAM_BYTE;
            ptr = memory->Get_Word_Pointer(str_to_num<Word>(trackedValue));
        }

        for (auto it = TrackedValues.begin(); it != TrackedValues.end(); it++) {
            if (std::get<0>(*it) == ptr && std::get<2>(*it) == type) {
                if (deleting) {
                    TrackedValues.erase(it);
                }
                else {
                    std::cerr << "Error! Value is already tracked.\n";
                }

                return KEEP_LOOPING;
            }
        }

        TrackedValues.emplace_back(ptr, *ptr);
        std::cout << "Value " << trackedValue << " is now tracked!\n";
    }

    else if (primary == "reg" || primary == "register") {
        static const std::vector<std::string> flagNames = {
            "Zero", "Subtraction", "Half-Carry", "Carry"
        };

        if (instructionParts.size() == 2) {
            std::string regName = instructionParts[1];

            for (char &c: regName) {
                c = static_cast<char>(std::toupper(c));
            }

            if (regName.length() == 2) {
                if (RegisterWordNames.count(regName)) {
                    std::cout << std::uppercase << regName << ": ";
                    std::cout << to_hex(*RegisterWordNames.at(regName)) << '\n';
                }
                else {
                    std::cerr << "Error! No register exists with that name.\n";
                }
            }
            else if (regName.length() == 1) {
                if (RegisterByteNames.count(regName)) {
                    std::cout << std::uppercase << regName << ": ";
                    std::cout << to_hex(*RegisterByteNames.at(regName)) << '\n';
                }
                else {
                    std::cerr << "Error! No register exists with that name.\n";
                }
            }
            else {
                std::cerr << "Error! No register exists with that name.\n";
            }
        }
        else {
            std::cout << "Registers:\n";

            for (auto const &r : RegisterWordNames) {
                std::cout << '\t' << r.first << ": " << to_hex(*r.second) << '\n';
            }

            std::cout << "Flags:\n";

            for (Byte i = 0; i < 4; ++i) {
                Byte bit = 7 - i;
                bool flagValue = ((registers.f) & (1 << bit)) > 0;

                std::cout << boost::format("\t%-11s: %d\n") % flagNames[i] % flagValue;
            }
        }
    }

    else if (primary == "x") {
        if (instructionParts.size() == 1) {
            std::cerr << "Error! Not enough arguments.\n";
            return KEEP_LOOPING;
        }

        Word start = str_to_num<Word>(instructionParts[1]);
        Word length = 1;

        if (instructionParts.size() == 3) {
            length = str_to_num<Word>(instructionParts[2]);
        }

        Byte firstLineOffset = start & 0xF;
        Word currentLine = start & 0xFFF0;

        Byte lines = static_cast<Byte>(((length + (firstLineOffset - 1)) & 0xFFF0) / 0x10);

        for (Byte l = 0; l <= lines; ++l) {
            Byte lineLength = 0x10;

            if (l == lines && (length + firstLineOffset) != 0x10) {
                Word sum = static_cast<Word>(length + firstLineOffset);

                if ((sum & 0xF) > 0) {
                    lineLength = sum & 0xF;
                }
                else {
                    lineLength = 0x10;
                }
            }

            std::cout << '\t' << to_hex(currentLine) << ":";

            for (Byte b = 0; b < lineLength; ++b) {
                std::cout << " ";

                if (l == 0 && b < firstLineOffset) {
                    std::cout << "__";
                }
                else {
                    std::cout << to_hex(memory->Read_Byte(currentLine + b), false);
                }
            }

            std::cout << "\n";
            currentLine += 0x10;
        }
    }

    else if (primary == "reset") {
        return RESET;
    }

    else if (primary == "exit") {
        return EXIT;
    }

    else {
        std::cerr << "Error! Unknown debug instruction.\n";
    }

    return KEEP_LOOPING;
}

void CPU::Poll_Registers() {
    auto it = RegisterFunctions.find(memory->LastWrittenTo);

    if (it != RegisterFunctions.end()) {
        (*it).second(*memory->Get_Byte_Pointer(memory->LastWrittenTo));
    }

    memory->LastWrittenTo = 0x0000;
}

void CPU::Dispatch_Loop() {
    while (true) {
        if (Exit) {
            return;
        }

        display->Process();

        if (CyclesUntilNextInstruction == 0) {
            switch (Run_Instruction()) {
                case RESET:
                    Reset();
                    continue;

                case EXIT:
                    return;
            }
        }

        clock.m += 1;
        clock.t += 4;

        CyclesUntilNextInstruction -= 1;

        if (memory->LastWrittenTo) {
            Poll_Registers();
        }
    }
}

void CPU::Compare(Byte a, Byte b) {
    registers.flagZero = (a == b);
    registers.flagSub = true;
    registers.flagHalf = (a & 0xF) < (b & 0xF);
    registers.flagCarry = a < b;
}

void CPU::Add(Byte &a, int8_t b, bool checkCarry) {
    Word value = a + b;

    registers.flagZero = ((value & 0xFF) == 0);
    registers.flagSub = (b < 0);

    if (checkCarry) {
        registers.flagCarry = value > 0xFF;
    }

    if (registers.flagSub) {
        registers.flagHalf = ((((a & 0xF) - ((-b) & 0xF)) & 0x10) == 0x10);
    }
    else {
        registers.flagHalf = ((((a & 0xF) + (b & 0xF)) & 0x10) == 0x10);
    }

    a = static_cast<Byte>(value & 0xFF);
}

void CPU::Add(Word &a, int16_t b, bool checkCarry) {
    int value = a + b;

    registers.flagZero = (value == 0);
    registers.flagSub = (b < 0);
    registers.flagHalf = ((((a & 0xFFF) + (b & 0xFFF)) & 0x1000) == 0x1000);

    if (checkCarry) {
        registers.flagCarry = value > 0xFFFF;
    }

    a = static_cast<Word>(value & 0xFFFF);
}
