#include "GameBoy.hpp"

#include <csignal>

#include <iostream>

static GameBoy gameboy;

void Set_Signal_Capture(int signal) {
	gameboy.Signal_Handler(signal);
}

int main() {
	struct sigaction sigIntHandler;

	sigIntHandler.sa_handler = Set_Signal_Capture;
	sigemptyset(&sigIntHandler.sa_mask);
	sigIntHandler.sa_flags = 0;

	sigaction(SIGINT, &sigIntHandler, NULL);

    gameboy.Initialize("/Users/mac/Downloads/GameBoy/DMG_ROM.bin");
    gameboy.Load_ROM("/Users/mac/Downloads/GameBoy/Tetris.gb");
    gameboy.Start();

    return 0;
}
