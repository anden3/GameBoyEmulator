#version 410 core

layout (location = 0) in int colorIndex;

const vec3 greyscalePalette[4] = vec3[4](
	vec3(0),
	vec3(0.33),
	vec3(0.66),
	vec3(1)
);

out vec3 Color;

uniform mat4 projection;

uniform int row;
uniform int colors[4];

void main() {
    gl_Position = projection * vec4(gl_VertexID, row, 0, 1);
    Color = greyscalePalette[colors[colorIndex]];
}
